class ApplicationMailer < ActionMailer::Base
  default from: '3929jarvis@gmail.com'
  layout 'mailer'
end
