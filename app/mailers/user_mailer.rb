class UserMailer < ApplicationMailer

	def OTPMailer email,otp,message,userName

		if email.blank? or otp.blank? or message.blank?  or userName.blank?
			return nil
		end

		@userName = userName
		@message  = message
		@otp = otp
		mail to:email, subject: 'OTP Verification' 
	end

	def passwordMailer email,message,password,userName


		if email.blank? or password.blank? or message.blank?  or userName.blank?
			return nil
		end
			
		@message = message
		@password = password
		@userName = userName
		mail to:email, subject: 'Pasword change'
	end
end

