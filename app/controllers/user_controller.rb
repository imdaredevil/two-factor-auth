class UserController < ApplicationController

	def home
		token = cookies[:userToken]

		if token.blank?
			render "home.html.erb"
			return
		end

		sessionInfo = SessionInformation.getSessionInfoByToken token
		if sessionInfo == nil
			render "home.html.erb"
			return
		elsif !sessionInfo.authenticated 
			render "verifyOTP.html.erb"
			return
		else
			currentUserId = sessionInfo.userId  
			if currentUserId.blank?
				sessionInfo.destroy
				redirect_to action:"home"
				return
			end


			currentUser = User.getUserById currentUserId
			if currentUser == nil or currentUser.userName.blank?
				sessionInfo.destroy
				redirect_to action:"home"
				return
			end

			@userName = currentUser.userName
			render "welcome.html.erb"
			return
		end
	end







	def changePassword
		token = cookies[:userToken]
		if token.blank?
			render "home.html.erb"
			return
		end

		sessionInfo = SessionInformation.getSessionInfoByToken token
		if sessionInfo == nil
			render "home.html.erb"
			return
		end

		if !sessionInfo.authenticated
			render "verifyOTP.html.erb"
			return
		end

		currentUserId = sessionInfo.userId

		if currentUserId.blank?
			sessionInfo.destroy
			redirect_to action:"home"
			return
		end

		currentUser = User.getUserById currentUserId
		if currentUser == nil
			sessionInfo.destroy
			redirect_to action:"home"
			return
		end


		if request.method == "POST"

			oldPassword = params[:oldPassword]
			newPassword = params[:newPassword]
			confirmPassword = params[:confirmPassword]


			if oldPassword.blank? or newPassword.blank? or confirmPassword.blank?
				@message = "all fields are required"
				render "changePassword.html.erb"
				return
			end

			if newPassword != confirmPassword 
				@message =  "new password and confirm password does not match"
				render "changePassword.html.erb"  
				return  
			end

			if !(currentUser.verifyPassword oldPassword)
				@message = "wrong original password"
				render "changePassword.html.erb"
				return
			end
			
			status = currentUser.changePassword newPassword
			if status != true
				render "changePassword.html.erb"  
				return  
			end

			@message = "change password success"
			render "welcome.html.erb"
		end
	end









	def forgotPassword
		if request.method == "POST"

			email = params[:email]
			if email.blank?
				@message = "all fields are required"
				render "forgotPassword.html.erb" 
				return
			end

			currentUser = User.getUserByEmail email
			if currentUser == nil or currentUser.email.blank?  or currentUser.userName.blank?
				@message = "email not registered"
				render "home.html.erb"  
				return   
			end

			randomPassword = SecureRandom.hex(5).to_s
			status = currentUser.changePassword randomPassword
			if status != true
				render "forgotPassword.html.erb"  
				return  
			end

			message = "This is your auto-generated password. Use this to login. Don't forget to change it"
			mail = UserMailer.passwordMailer  currentUser.email,message,randomPassword,currentUser.userName	
			if mail == nil
				render "forgotPassword.html.erb"  
				return  
			end

			mail.deliver
			
			@message = "Your new password is sent to your mail"
			render "home.html.erb"
		end
	end











	def resendOTP
		token = cookies[:userToken]
		if token.blank?
			redirect_to action:"home"
			return
		end

		sessionInfo = SessionInformation.getSessionInfoByToken token
		if sessionInfo == nil
			redirect_to action:"home"
			return
		end

		if sessionInfo.authenticated
			redirect_to action:"home"
			return
		end

		currentUserId = sessionInfo.userId

		if currentUserId.blank?
			sessionInfo.destroy
			redirect_to action:"home"
			return
		end

		currentUser = User.getUserById currentUserId

		if currentUser == nil or currentUser.email.blank?  or currentUser.userName.blank?
			sessionInfo.destroy
			redirect_to action:"home"
			return
		end

		otp = OneTimePassword.generateOtp currentUserId
		if otp.blank?
			redirect_to action:"home"
			return
		end

		message = "Verify your account by entering the below given OTP. We have re-sent it for you"
		mail = UserMailer.OTPMailer  currentUser.email,otp,message,currentUser.userName	
		if mail == nil
			@message = "try again"
			render "verifyOTP.html.erb"  
			return  
		end

		mail.deliver

		@message = "mail sent again"
		render "verifyOTP.html.erb"	
	end












	def logout

		token = cookies[:userToken]
		if token != nil
			sessionInfo = SessionInformation.getSessionInfoByToken token
			if sessionInfo != nil
				sessionInfo.destroy
			end
			cookies.delete :userToken
		end
		redirect_to action:"home"
	end

	def signin

		if request.method == "POST"
			
			email = params[:email]
			password = params[:password]

			if email.blank? or password.blank?
				@message = "all fields are required"
				render "signin.html.erb" 
				return
			end

			currentUser = User.getUserByEmail email
			if currentUser == nil or currentUser.id.blank?
				@message = "email not registered"
				render "home.html.erb"  
				return   
			end
			
			if currentUser.verifyPassword password
				otp = OneTimePassword.generateOtp currentUser.id,true
				token = SessionInformation.generateToken currentUser.id
				if otp.blank? or token.blank?
					@message  = "some internal error. Try again"
					render "signin.html.erb"  
					return
				end  
				cookies[:userToken] = token
				message = "Verify your account by entering the below given OTP so that you can log in to your account"
				mail = UserMailer.OTPMailer  email,otp,message,currentUser.userName
				if mail == nil
					@message  = "some internal error. Try again"
					render "signin.html.erb"  
					return
				end  
				mail.deliver
				render "verifyOTP.html.erb"	
			else
				@message = "wrong password"
				render "signin.html.erb"  
				return    
			end
		end
	end


















	def verifyOTP

		otp = params[:OTP]
		if otp.blank?
			@message= "All fields must be present"
			render "verifyOTP.html.erb" 
			return
		end

		token = cookies[:userToken]
		if token.blank?
			redirect_to action: "home"
			return
		end

		sessionInfo = SessionInformation.getSessionInfoByToken token 
		if sessionInfo==nil
			@message = "your session is expired.Login again"
			render "home.html.erb"
			return
		end

		currentUserId = sessionInfo.userId
		if currentUserId.blank?
			sessionInfo.destroy
			redirect_to action: "home" 
			return
		end	

		status = OneTimePassword.authenticate otp,currentUserId
		#puts status
		if status.blank? or status=="VERIFIED"
			
			authenticateStatus  = SessionInformation.authenticate token
			if authenticateStatus != true
				sessionInfo.destroy
				redirect_to action: "home" 
				return
			end
			
			redirect_to action: "home"
			return

		elsif status=="EXPIRED"
			@message = "your OTP expires. Kindly login again"
			render "signin.html.erb"
			return 
		elsif status=="WRONG"
			@message = "wrong OTP. try again"
			render "verifyOTP.html.erb"
			return 
		end		
	end













	def signup
		if request.method == "POST"

			userName = params[:name]
			email = params[:email]
			password = params[:password]
			confirmPassword = params[:confirmPassword]

			#puts userName
			if userName.blank? or email.blank? or password.blank? or confirmPassword.blank?
				@message = "all fields are required"
				render "signup.html.erb" 
				return
			end

			if password != confirmPassword 
				@message =  "password and confirm password does not match"
				render "signup.html.erb"  
				return  
			end

			if (User.getUserByEmail email) != nil
				@message = "email already registered"
				render "signup.html.erb"  
				return  
			end

			newUser = User.create userName,email,password
			if newUser == nil or newUser.id.blank?
				@message  = "some internal error. Try again"
				render "signup.html.erb"  
				return
			end  

			otp = OneTimePassword.generateOtp newUser.id,false
			token = SessionInformation.generateToken newUser.id
			if otp.blank? or token.blank?
				@message  = "some internal error. Try again"
				render "signup.html.erb"  
				return
			end  
			cookies[:userToken] = token
			message = "Welcome to two-factor-auth.Verify your account by entering the below given OTP.Only then your account will be confirmed"
			mail = UserMailer.OTPMailer  email,otp,message,userName
			if mail == nil
				@message  = "some internal error. Try again"
					render "signup.html.erb"  
					return
				end  	
			mail.deliver
			render "verifyOTP.html.erb"		 
		end
		
	end
end
