class OneTimePassword < ApplicationRecord
	validates_presence_of :userId

	def self.generateOtp userId,doesExpire=nil

		deleteExpired

		if userId.blank?
			return nil
		end

		otp = self.find_by(userId: userId)
		if otp.blank?
			otp = self.new
			otp.userId  = userId
		end

		present = Time.now.to_time.to_i
		expiry = otp.expiryAt
		#puts present
		#puts expiry
		if expiry.blank? or otp.otp.blank? or expiry <= present
			#puts "updated"
			otp.otp = rand(100000..999999)
			#puts otp.expiryAt
			if doesExpire!=nil
				otp.doesExpires = doesExpire
			end
		end
		
		otp.expiryAt = (present + 10*60) 
		otp.save
		return otp.otp
	end

	def self.authenticate otp,userId

		deleteExpired
		
		if otp.blank? or userId.blank?
			return nil
		end

		otp = otp.to_i

		otpRecord  = self.find_by(userId: userId)
		if otpRecord==nil or otpRecord.otp.blank?
			return "EXPIRED"
		end

		expiry = otpRecord.expiryAt
		present = Time.now.to_time.to_i
		#puts otpRecord.otp
		#puts present
		#puts expiry
		if (!expiry.blank?) and otpRecord.doesExpires and expiry <= present
			return "EXPIRED"
		end

		if otpRecord.otp != otp
			return "WRONG"
		end

		if otpRecord.otp == otp
			otpRecord.expiryAt = (present - 60*60*10)
			otpRecord.save
			return "VERIFIED"
		end 
	end

	def self.deleteExpired
		expiredentries = OneTimePassword.where("expiryAt <= ? ",Time.now.to_time.to_i)
		expiredentries.destroy_all
	end

end
