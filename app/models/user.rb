class User < ApplicationRecord
	validates_presence_of :userName
	validates_presence_of :password
	validates_presence_of :email

	def self.create name,email,password
			if name.blank? or email.blank? or password.blank?
				return nil
			end
			
			newUser = self.new
			newUser.userName = name
			newUser.email = email
			newUser.password = self.hasher password
			
			if newUser.password.blank?
				return nil
			end

			newUser.confirmed = false
			newUser.save
			return newUser
	end

	def self.getUserById userId
		
		if userId.blank?
			return nil
		end

		user  = User.find_by(id: userId)

		 if user == nil
		 	return nil
		 end

		 return user
	end

	def self.getUserByEmail email
		
		if email.blank?
			return nil
		end

		user  = User.find_by(email: email)

		 if user == nil
		 	return nil
		 end

		 return user
	end	


	def self.hasher password
		if password.blank?
			return nil
		end
		BCrypt::Password.create(password).to_s
	end


	def changePassword password

		if password.blank?
			return nil
		end

		self.password = User.hasher password
		self.save
		return true
	end


	def verifyPassword password

		if password.blank?
			return false
		end

 		if self.password and BCrypt::Password.new(self.password) == password
 			return true
 		else
 			return false
 		end		
	end
end
