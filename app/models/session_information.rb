class SessionInformation < ApplicationRecord

	def self.generateToken userId

		deleteExpired

		if userId.blank?
			return nil
		end

		sessionInfo = self.find_by(userId: userId)
		if sessionInfo == nil
			sessionInfo = self.new
			sessionInfo.userId  = userId
		end

		present = Time.now.to_time.to_i
		expiry = sessionInfo.expiryAt
		if expiry.blank? or sessionInfo.userToken.blank? or expiry <= present
			sessionInfo.userToken = SecureRandom.hex
			sessionInfo.expiryAt = present + 24*60*60
			sessionInfo.authenticated = false
			sessionInfo.save
		end

		return sessionInfo.userToken
	end

	def self.authenticate token

		deleteExpired

		if token.blank?
			return nil
		end

		sessionInfo = self.find_by(userToken: token)
		if sessionInfo == nil
			return nil
		end
		present = Time.now.to_time.to_i
		sessionInfo.expiryAt = present + 24*60*60
		sessionInfo.authenticated = true
		sessionInfo.save
	end


	def self.getSessionInfoByToken token

		deleteExpired

		if token.blank?
			return nil
		end

		sessionInfo = self.find_by(userToken: token)
		if sessionInfo == nil
			return nil
		end

		present = Time.now.to_time.to_i
		expiry = sessionInfo.expiryAt
		#puts expiry
		#puts present
		if expiry.blank? or sessionInfo.userToken.blank? or expiry <= present
			return nil
		end
		sessionInfo.expiryAt = (present + 24*60*60)
		sessionInfo.save
		return sessionInfo
	end

	def self.deleteExpired
		expiredentries = SessionInformation.where("expiryAt <= ? ",Time.now.to_time.to_i)
		expiredentries.destroy_all
	end


end
