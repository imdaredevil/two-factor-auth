Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get 'signin' => "user#signin"
  get 'signup' => "user#signup"
  get 'logout' => "user#logout"
  get 'resendOTP' => "user#resendOTP"
  get 'forgotPassword' => "user#forgotPassword"
  get 'changePassword' => "user#changePassword"

  post 'signin' => "user#signin"
  post 'signup' => "user#signup"
  post 'verifyOTP' => "user#verifyOTP"
  post 'forgotPassword' => "user#forgotPassword"
  post 'changePassword' => "user#changePassword"

  root :to => "user#home"
end


