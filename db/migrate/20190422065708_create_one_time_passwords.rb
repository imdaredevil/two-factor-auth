class CreateOneTimePasswords < ActiveRecord::Migration[5.2]
  def change
    create_table :one_time_passwords do |t|
      t.integer :otp
      t.integer :userId
      t.integer :expiryAt

      t.timestamps
    end
  end
end
