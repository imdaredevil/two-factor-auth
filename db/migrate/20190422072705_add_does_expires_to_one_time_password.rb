class AddDoesExpiresToOneTimePassword < ActiveRecord::Migration[5.2]
  def change
    add_column :one_time_passwords, :doesExpires, :boolean
  end
end
