class CreateSessionInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :session_informations do |t|
      t.string :userToken
      t.boolean :authenticated
      t.integer :userId
      t.integer :expiryAt

      t.timestamps
    end
  end
end
