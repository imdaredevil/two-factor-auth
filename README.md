# README

This is a website that demonstrates how two factor authentication is done using mail. 


**Dependencies**


*  Ruby 2.5.1

*  Rails 5.2

*  mysql
    

**Configuration**


*  After cloning the repository, run bundle install to install the gems
    

*  Run rake db:create and db:migrate to make the necessary migrations
    

*  The email and password used to send mails is encrypted using credentials gem and master.key required to decrypt is not present in the repository. 
    

*  So change the email and password to send mail in config/environments/production.rb
    

*  To test whether a mail is being actually sent, run the server in production mode


**Deployment**

    

*  The website is hosted in aws using nginx and phusion

*  The HTTPS Certificate is provided by LetsEncrypt

*  The hosted website can be found at [TwofactorAuth](https://home.twoway-auth.cf/)
    
    
    


